var express = require('express');
var router = express.Router();
var passport = require("passport");
var FacebookStrategy = require('passport-facebook').Strategy;
var user = {}
var app = express();


/* 

http://passportjs.org


ESTOY USANDO UNA LIBRERIA LLAMADA PASSPORT JS CON LA QUE PUDO ACCEDER A LA API DE FACEBOOK PARA INGRESAR
1. SE CREA UN APP DENTRO DEL AREA DE DESARROLLADORES DEL FACEBOOK PARA ASI ACCEDER A SU API
2. FACEBOOK DA UN clientID y un clientSecret QUE SON DOS DATOS PARA IDENTIFICAR A QUE APP VAS A INGRESAR
3. OBTENIENDO ESTO PASSPORT HACE EL CONTACTO CON EL API SOLAMENTE PIDIENDO LOS DATOS QUE SE NECESITAN
EN ESTE CASO SOLAMENTE PIDO EL Nombre, Email, y la URL de la foto, CON ESTO EL USUARIO YA SOLO 
PRECIONA EN EL BOTON Y LOGRA ENTRAR DESDE EL API Y NOSOTROS PODEMOS REGISTRARLE EN LA BASE DE DATOS CON
LOS DATOS QUE FACEBOOK NOS DA

 */
  passport.use(new FacebookStrategy({
    clientID:"238927383277518", 
    clientSecret: "522ccb409ae5188dc3babe54d9842b99",
    callbackURL: "http://localhost/auth/facebook/callback",
    profileFields:["id", "displayName", "emails", "photos"] 
  }, function(accessToken, refreshToken, profile, cb){
    //En esta parte se obtienen los datos que el API nos da y lo registramos en la base de datos
    //De devolver los datos se almacenan en este caso en una variable local y se muestra en la pagina
    if(profile){
    user.Nombre = profile.displayName
    user.Email = profile.emails[0].value;
    user.Photo = profile.photos[0].value;
    //LUEGO SE MANDA A LLAMAR EL CALLBACK QUE ES DONDE SE DESIGNA SI ESTAN BIEN LOS DATOS O SI ESTAN MAL
    //LE ENVIO EL PROFILE PARA INDICAR QUE SI VINIERON BIEN LOS DATOS
      cb(null, profile, null);
    }else{
    //DE ESTAR MAL SE INIDICA QUE ESTA AL EL DATO      
      cb('404', null, "Usuario no Encontrado");
    }
  }));


router.get('/facebook/index',function(req, res, next){
  res.render("Facebook/facebookIndex",{title:"Facebook Index", user:user});
});

router.get('/facebook/error',function(req, res, next){
  res.render("Facebook/facebookError",{erro:"404", message:"Usuario no encontrado"});
});

//ACA SE MANDA A LLAMAR EL API DE FACEBOOK PIDIENDO DATOS BASICOS Y EL EMAIL
router.get('/auth/facebook',
  passport.authenticate('facebook' , {scope: ['public_profile','email']}));


//ACA SE RECIBEN LOS DATOS Y SE EVALUAN SI FUE BUENA  MALA LA CONSULTA
router.get('/auth/facebook/callback', function(req, res, next){
  passport.authenticate("facebook", function(err, profile, options) {

		if (err) {
      return res.redirect("/facebook/error?error=500&mesagge="+err.message);}

    if (!profile) {
      return res.redirect("/facebook/error?error="+err+"&message="+options);}
    
    if(profile) {
      return res.redirect("/facebook/index/"); }

	})(req, res, next);

  });
module.exports = router;
