CREATE DATABASE axaControl;
USE axaControl;

CREATE TABLE tipoUsuario(
    idTipoUsuario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(20)
);

CREATE TABLE usuario(
    idUsuario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(60) NOT NULL,
    edad INT NOT NULL,
    fechaCreacion DATETIME NOT NULL,
    nick VARCHAR(20) NOT NULL,
    contrasena VARCHAR(20) NOT NULL,
    urlIMG VARCHAR(100)  NOT NULL,
    idTipoUsuario INT NOT NULL,
    FOREIGN KEY (idTipoUsuario) REFERENCES tipoUsuario(idTipoUsuario)
);

CREATE TABLE auto(
    idAuto INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    idUsuario INT NOT NULL,
    modelo VARCHAR(20) NOT NULL,
    marca VARCHAR(20) NOT NULL,
    anio VARCHAR(4) NOT NULL,
    fechaCreacion DATETIME NOT NULL,
    FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario)
);

CREATE TABLE servicio(
    idServicio INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    idMecanico INT NOT NULL,
    idAuto INT NOT NULL,
    fecha DATETIME NOT NULL,
    descripcion TEXT NOT NULL,
    FOREIGN KEY (idMecanico) REFERENCES usuario(idUsuario),
    FOREIGN KEY (idAuto) REFERENCES auto(idAuto)
);

/* TABLA PARA COMENTARIOS DEL SERVICIO 
    SE USA HACIENDO DOS CONSULTAS INDEPENDIENTES 
    UNA HACIA EL MECANICO 
    OTRA PARA EL USUARIO 
    AMBAS CON SUS JOINS
*/
CREATE TABLE detalleServicio(
    idDetalleS INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    idMecanico INT NOT NULL,
    idCliente INT NOT NULL,
    descripcion TEXT NOT NULL,
    FOREIGN KEY (idMecanico) REFERENCES usuario(idUsuario),
    FOREIGN KEY (idCliente) REFERENCES usuario(idUsuario)
);

CREATE TABLE factura(
    idFactura INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    idServicio INT NOT NULL,
    fecha DATETIME NOT NULL,
    idUsuario  INT NOT NULL,
    total DECIMAL(8,2) NOT NULL,
    FOREIGN KEY (idServicio) REFERENCES servicio(idServicio),
    FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario)
);

/* TABLA PARA COMENTARIOS DE MECANICOS | ADMINISTRADORES 
    SE USA HACIENDO DOS CONSULTAS INDEPENDIENTES 
    UNA HACIA EL MECANICO 
    OTRA PARA EL USUARIO | ADMINISTRADORES 
    AMBAS CON SUS JOINS
*/
CREATE TABLE detalleUsuario(
    idDetalleU INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    idUsuario INT NOT NULL,
    idCliente INT NOT NULL,
    descripcion TEXT NOT NULL,
    FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario),
    FOREIGN KEY (idCliente) REFERENCES usuario(idUsuario)
);

CREATE TABLE calendario(
    idCalendario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    idCliente INT NOT NULL,
    fecha DATETIME NOT NULL,
    idMecanico INT NOT NULL,
    descripcion TEXT NOT NULL,
    FOREIGN KEY (idCliente) REFERENCES usuario(idUsuario),
    FOREIGN KEY (idMecanico) REFERENCES usuario(idUsuario)
);

/*
    ____________     _____   _____       ____
    |           |   |    |   |     \     |  |      
    |    _______|   |    |   |      \    |  |
    |   |___        |    |   |       \   |  |
    |   ____|       |    |   |        \  |  |
    |   |           |    |   |         \ |  |
    |   |           |    |   |    |\    \|  |
    |   |           |    |   |    | \       |
    |___|           |____|   |____|  \______| ALV 
*/